from LxmlSoup import LxmlSoup
import requests

html = requests.get('https://habr.com/ru/search/page1/?q=%5Bpython%5D&target_type=posts&order=relevance').text  # получаем html код сайта
#print(html)
soup = LxmlSoup(html)  # создаём экземпляр класса LxmlSoup

#links = soup.find_all('a', class_='tm-article-datetime-published tm-article-datetime-published_link')  # получаем список ссылок и наименований
links = soup.find_all('a', class_='tm-title__link')  # получаем список ссылок и наименований
for i, link in enumerate(links):
    url = link.get("href")  # получаем ссылку товара
    name = link.text()  # извлекаем наименование из блока со ссылкой
#    price = soup.find_all("div", class_="tm-title__link")[i].text()  # извлекаем цену
#    print(i)
    print(f"Url - https://habr.com{url}")
    print(f"Name - {name}")
#    print(f"Price - {price}\n")

